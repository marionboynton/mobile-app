/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import { WebView } from 'react-native-webview';

const styles = StyleSheet.create({
  header: {
    paddingTop: 50,
    paddingBottom: 10,
    backgroundColor: "#32CD32"
  },
  title: {
    color: "#fff",
    fontSize: 35,
    fontWeight: "bold",
    textAlign: "center"
  },
  icon: {
    color: "#fff",
    fontSize: 20,
    fontWeight: "bold",
    textAlign: "center"
  },
  footer: {
    padding: 20,
    flexDirection: "row",
    justifyContent: "space-around",
    backgroundColor: "#0c084c"
  }
});


const Header = () => (
  <View style={styles.header}>
    <Text style={styles.title}>Balancell</Text>
  </View>
);

export default class MyWeb extends Component {
  render() {
    return (
      <View style={{ flex: 1}}>
      <Header />
      <WebView
        source={{ uri: "http://192.168.4.1" }}
        style={{ marginTop: 20 }}

      />
      </View>
    );
  }
}
